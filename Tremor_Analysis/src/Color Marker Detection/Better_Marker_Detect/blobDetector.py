import cv2
import numpy as np
import os
import pdb

from dist import dist


#______________________________________________________
#
# Function: pruneKeypoints
#
# 			get one keypoint at each location (ignore multiple dections)
# \param[in] keypoints: 	keypoints
# \return good_keypoints:		keypoints, require at distance of at least 2 pixels 
#							between keypoints
#_______________________________________________________
def pruneKeypoints(keypoints):
	# min distance between keypoints in pixels
	good_threshold = 11

	# max distance between keypoints
	#max_good_threshold = 200
	# list of unique keypoints
	good_keypoints = []
	# start by adding first keypoint
	good_keypoints.append(keypoints[0])
	# compare each keypoint
	for keypt in keypoints:
		goodFlag = True
		# to each point already in good_keypoints 
		point = keypt.pt
		for good_kp in good_keypoints:
			good = good_kp.pt
			if(dist(point,good) < good_threshold):
				goodFlag = False
			#if dist(point,good) > max_good_threshold:
			#	goodFlag = False
		# if dist > 2 for all points already in good_keypoints
		if(goodFlag):
			# append the keypoint
			good_keypoints.append(keypt)

	return good_keypoints

#_________________________________________________________
#______________________________________________________
# 
# Function: MSERblobDetector
# 
#			find blobs in input image
# \params[in] image: 		probability image 
# \params[in] min_area: 	minimum marker blob area
# \params[in] max_area: 	maximum marker blob area
# \return:	 keypoints:		blob locations and radii in image
#
# Reference: https://www.learnopencv.com/blob-detection-using-opencv-python-c/
#_______________________________________________________
def MSERBlobDetector(image, min_area, max_area):
	# make image 1 channel
	if(len(image.shape) > 2):		
		gray = cv2.cvtColor(np.uint8(image), cv2.COLOR_BGR2GRAY)
	else: 
		# gray = np.uint8(image)
		gray = image
	

	#### OG  Below #####

	# ##Panth Debugging to show the maskedImages

	# #cv2.imshow('MSERBlobDetector Input', image)
	# #cv2.waitKey(0)
	# #cv2.destroyAllWindows()
	# # smooth image 
	# gray = cv2.GaussianBlur(gray, (3,3), 3)
	# gray = cv2.GaussianBlur(gray, (5,5), 3)
	# gray = cv2.GaussianBlur(gray, (7,7), 3)

	# # smooth image more
	# gray = cv2.GaussianBlur(gray, (3,3), 2)
	# retval, gray = cv2.threshold(gray, 150, 255, 0)

	# # create MSER detector
	# mser = cv2.MSER_create()
	# # set min blob area 
	# mser.setMinArea(min_area)
	# # set max blob area 
	# mser.setMaxArea(max_area)
	# print("image.shape: " + str(image.shape))
	
	# # detect regions in image
	# regions = mser.detect(gray)
	
	# if len(regions) > 0:
	# 	# only get one keypoint at each location
	# 	keypoints = pruneKeypoints(regions)
	# else:
	# 	keypoints = regions

	# points = []
	# for kp in keypoints:
	# 	points.append(kp.pt)

	##### OG ABOVE ####

	# ##Panth Testing
	# #regions, _ = mser.detectRegions(gray)
	# #hulls = [cv2.convexHull(p.reshape(-1, 1, 2)) for p in regions]
	# #cv2.polylines(vis, hulls, 1, (0, 255, 0))

	# #hulls = [cv2.convexHull(p.reshape(-1, 1, 2)) for p in regions]
	# #cv2.polylines(vis, hulls, 1, (0, 255, 0))
	# #cv2.imshow('img', vis)
	# #cv2.waitKey(0)
	# #cv2.destroyAllWindows()

	kernel3 = np.ones((1,1),np.uint8)
	kernel5 = np.ones((3,3),np.uint8)
	kernel9 = np.ones((5,5),np.uint8)
	kernel6 = np.ones((6,6),np.uint8)
	kernel21 = np.ones((9,9),np.uint8)
	# gray = cv2.morphologyEx(image, cv2.MORPH_CLOSE, kernel5)
	# gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel5)
	# gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel9)
	# gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel9)
	# gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel9)
	# gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel9)
	# # smooth image 
	# gray = cv2.GaussianBlur(gray, (3,3), 3)
	# gray = cv2.GaussianBlur(gray, (5,5), 3)
	# #gray = cv2.GaussianBlur(gray, (7,7), 3)

	# # do closing to close holes in connected component
	# gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel5)
	# gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel5)
	# gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel9)
	# gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel9)

	# # create MSER detector
	# mser = cv2.MSER_create()
	# # pdb.set_trace()
	# # if key == "marker":
	# # 	# # # set max blob area 
	# # 	mser.setMaxArea(2200)

	# # elif key == "tool":
	# # 	gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel9)
	# # 	gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel21)
	# # 	gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel21)
	# # 	gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel9)

	# # 	# smooth image more
	# # 	gray = cv2.GaussianBlur(gray, (3,3), 2)
	# # 	retval, gray = cv2.threshold(gray, 150, 255, 0)
	# # 	mser.setMinArea(200)
	# # 	mser.setMaxArea(800)

	# gray = cv2.GaussianBlur(gray, (3,3), 2)
	# retval, gray = cv2.threshold(gray, 150, 255, 0)
	# mser.setMinArea(20)
	# mser.setMaxArea(800)
	# elif key == "marker_template":


	retval,gray = cv2.threshold(image,220,255,cv2.THRESH_TRUNC)    #Best so far
	#gray = cv2.adaptiveThreshold(gray,,cv2.ADAPTIVE_THRESH_MEAN_C,\
	#cv2.THRESH_BINARY,5,2)
	          
	gray = cv2.GaussianBlur(gray, (1,1), 2)
	#gray = cv2.morphologyEx(gray, cv2.MORPH_GRADIENT, kernel3)
	gray = cv2.morphologyEx(gray, cv2.MORPH_TOPHAT, kernel6)
	gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel3)       
	gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel3)         
	gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel5)
	gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel6)
	gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel5)
	gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel5)
	gray = cv2.GaussianBlur(gray, (1,1), 2)
	#gray = cv2.morphologyEx(gray, cv2.MORPH_TOPHAT, kernel6)
	#gray = cv2.morphologyEx(gray, cv2.MORPH_OPEN, kernel)


	mser = cv2.MSER_create()
	print("image.shape: " + str(image.shape))
	mser.setMaxArea(max_area)
	mser.setMinArea(min_area)


	print("image.shape: " + str(image.shape))

	# detect regions in image
	keypoints = mser.detect(gray)

	# copy the image to draw on
	blobImage = image.copy()
	if(len(keypoints) > 0):
		keypoints = pruneKeypoints(keypoints)

	#blobImage = cv2.drawKeypoints(gray, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

	#cv2.imshow('MSERBlobDetector Output', blobImage)
	#cv2.waitKey(0)
	#cv2.destroyAllWindows()

	points = []
	for kp in keypoints:
		points.append(kp.pt)

			# return blobImage, keypoints, points

	return keypoints

#______________________________________________________
# 
# Function: MSERblobDetector
# 
#			find blobs in input image
#
# \params[in] image: 	probability image 
# \params[in] key: 		string indicating "tool" or "marker"
# \return 	keypoints: 	blob keypoints
# \return	points:		center of blob locations in image
# \return 	blobImage: 	image with detected blobs drawn on image
#
# Reference: https://www.learnopencv.com/blob-detection-using-opencv-python-c/
# #_______________________________________________________
def MSERBlobDetectorDebugging(image, key):
	# convert image to grayscale if color image
	if(len(image.shape) > 2):		
		image = cv2.cvtColor(np.uint8(image), cv2.COLOR_BGR2GRAY)

	#cv2.imshow("image", image)
	#pdb.set_trace()
	#gray = image
	kernel3 = np.ones((3,3),np.uint8)
	kernel5 = np.ones((5,5),np.uint8)
	kernel9 = np.ones((9,9),np.uint8)
	kernel21 = np.ones((21,21),np.uint8)
	gray = cv2.morphologyEx(image, cv2.MORPH_CLOSE, kernel5)
	#cv2.imshow("gray",gray)
	# gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel5)
	# gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel9)
	# gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel9)
	# gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel9)
	# gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel9)
	# # smooth image 
	# gray = cv2.GaussianBlur(gray, (3,3), 3)
	# gray = cv2.GaussianBlur(gray, (5,5), 3)
	# gray = cv2.GaussianBlur(gray, (7,7), 3)

	# # do closing to close holes in connected component
	# gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel5)
	# gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel5)
	# gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel9)
	# gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel9)

	# # create MSER detector
	mser = cv2.MSER_create()
	# # pdb.set_trace()
	# if key == "marker":
		# # # set max blob area 
	mser.setMaxArea(1000)

	# elif key == "tool":
	# 	gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel9)
	# 	gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel21)
	# 	gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel21)
	# 	gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel9)

	# 	# smooth image more
	# 	gray = cv2.GaussianBlur(gray, (3,3), 2)
	retval, gray = cv2.threshold(gray, 150, 255, cv2.THRESH_TRUNC)
	gray = cv2.morphologyEx(gray,cv2.MORPH_CLOSE,kernel5)
	# mser.setMinArea(1)
	# 	mser.setMaxArea(4800)

	# elif key == "marker_template":


	print("image.shape: " + str(image.shape))

	# detect regions in image
	keypoints = mser.detect(gray)	

	# copy the image to draw on
	blobImage = image.copy()
	if(len(keypoints) > 0):
		keypoints = pruneKeypoints(keypoints)

	blobImage = cv2.drawKeypoints(gray, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

	#cv2.imshow("blobImage",blobImage)

	points = []
	for kp in keypoints:
		points.append(kp.pt)

	print("keypoints: ", keypoints)
			# return blobImage, keypoints, points
	return keypoints, points, blobImage
#______________________________________________________

#Function for tool blobs
#______________________________________________________
def MSERBlobDetectortool(image, min_area, max_area):
	kernel = np.ones((1,1),np.uint8)

	if(len(image.shape) > 2):        
	        image = cv2.cvtColor(np.uint8(image), cv2.COLOR_BGR2GRAY)

	gray = image
	kernel3 = np.ones((3,3),np.uint8)
	kernel5 = np.ones((5,5),np.uint8)
	kernel6 = np.ones((6,6),np.uint8)
	kernel9 = np.ones((9,9),np.uint8)
	kernel21 = np.ones((21,21),np.uint8)
	#gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel3)
	#gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel3)

	# =============================================================================
	# 
	# ## Panth Debugging All Commenting Out
	# 
	# gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel9)
	# gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel9)
	# gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel9)
	# gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel9)
	# =============================================================================

	# =============================================================================
	# # smooth image ##Panth Debugging
	    #gray = cv2.GaussianBlur(gray, (3,3), 3)
	# gray = cv2.GaussianBlur(gray, (5,5), 3)
	# gray = cv2.GaussianBlur(gray, (7,7), 3)
	# 
	# =============================================================================
	# =============================================================================
	# # do closing to close holes in connected component
	# gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel5)
	# gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel5)
	# gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel9)
	# gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel9)
	# 
	# =============================================================================
	# create MSER detector
	mser = cv2.MSER_create()
	# pdb.set_trace()
	#if key == "marker":
	    # # # set max blob area 
	    #mser.setMaxArea(2200)

	#elif key == "tool":
	    #gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel9)
	    #gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel21)
	    #gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel21)

	# =============================================================================
	# 
	# ## tool specific alterations
	# gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel9)
	# gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel21)
	# gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel21)
	# 
	# =============================================================================

	    # smooth image more
	#gray = cv2.GaussianBlur(gray, (3,3), 2)
	    
	#gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel5)
	#gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel3)
	#gray = cv2.GaussianBlur(gray, (3,3), 3)
	#gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel5)
	#gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel5)
	#gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel5)
	## Options for threshoding

	#retval, gray = cv2.threshold(gray, 150, 255, 0)
	#retval,gray = cv2.threshold(image,150,255,cv2.THRESH_TOZERO_INV) #Decent for bobs
	retval,gray = cv2.threshold(image,200,255,cv2.THRESH_TRUNC)    #Best so far
	#gray = cv2.adaptiveThreshold(gray,,cv2.ADAPTIVE_THRESH_MEAN_C,\
	#cv2.THRESH_BINARY,5,2)
	          
	gray = cv2.GaussianBlur(gray, (1,1), 2)
	#gray = cv2.morphologyEx(gray, cv2.MORPH_GRADIENT, kernel3)
	gray = cv2.morphologyEx(gray, cv2.MORPH_TOPHAT, kernel6)
	gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel3)       
	gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel3)         
	gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel5)
	gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel6)
	gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel5)
	gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel5)
	gray = cv2.GaussianBlur(gray, (1,1), 2)
	#gray = cv2.morphologyEx(gray, cv2.MORPH_TOPHAT, kernel6)
	#gray = cv2.morphologyEx(gray, cv2.MORPH_OPEN, kernel)



	print("image.shape: " + str(image.shape))
	mser.setMaxArea(max_area)
	mser.setMinArea(min_area)

	# detect regions in image
	keypoints = mser.detect(gray)

	# copy the image to draw on
	blobImage = image.copy()
	if(len(keypoints) > 0):
	    keypoints = pruneKeypoints(keypoints)

	blobImage = cv2.drawKeypoints(gray, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
	
	
	#blobProbImage = cv2.drawKeypoints(prob_im, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

	#outIm = np.concatenate((blobImage, blobProbImage), axis=1)
		# show image and prob_im with blob detections
	#cv2.imshow("blobImage", blobImage) 
	#cv2.waitKey(0)
		
	# =============================================================================
	# ## Cany Detection
	# edges = cv2.Canny(image,50,250)
	# keypoints_canny = mser.detect(edges)
	# 
	# if(len(keypoints_canny) > 0):
	#     keypoints_canny = pruneKeypoints(keypoints_canny)
	#     
	# blobImage_canny = cv2.drawKeypoints(edges, keypoints_canny, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
	# 
	# =============================================================================
	#mser.setMinArea(200)
	#mser.setMaxArea(800)
	    
	# print("Done")
	# print("1")
	# cv2.imshow('image',gray)
	# #cv.imshow('image',img)
	# cv2.waitKey(0)
	# print("2")
	# cv2.imshow('2', blobImage)
	# cv2.waitKey(0)
	# =============================================================================
	# print("3")
	# cv2.imshow('3', edges)
	# cv2.waitKey(0)
	# print("4")
	# cv2.imshow('4', blobImage_canny)
	# cv2.waitKey(0)
	# =============================================================================
	# cv2.destroyAllWindows()

	return keypoints